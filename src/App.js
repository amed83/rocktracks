import React from "react";
import Dashboard from "./containers/dashboard/Dashboard";

function App() {
  return (
    <div>
      <Dashboard data-test="dashboard-component" />
    </div>
  );
}

export default App;
