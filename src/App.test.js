import App from "./App";

test("renders Dashboard component", () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find('[data-test="dashboard-component"]')).toHaveLength(1);
});
