import React from "react";
import { MainContainer } from "./style";

function AlertMessage({ alertMessage }) {
  return (
    <MainContainer>
      <h2>{alertMessage}</h2>
    </MainContainer>
  );
}

export default AlertMessage;
