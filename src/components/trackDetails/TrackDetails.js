import React from "react";
import {
  MainContainer,
  DetailsContainer,
  ImageAndBtnContainer,
  StyledLinkedButton,
  StyledButton,
} from "./style";
import {
  convertFromMilliseconds,
  convertDate,
} from "../../utils/convertTime/convertTime";

function TrackDetails({ ...props }) {
  const {
    isDetailsPage,
    getSingleTrackDetails,
    trackViewUrl,
    trackId,
    trackTimeMillis,
    releaseDate,
    trackName,
    artistName,
    currency,
    trackPrice,
  } = props;

  const detailsButton = () => {
    if (isDetailsPage) {
      return (
        <StyledButton
          data-test="normal-btn"
          onClick={() => window.open(trackViewUrl)}
        >
          More Details
        </StyledButton>
      );
    } else {
      return (
        <StyledLinkedButton
          data-test="linked-btn"
          onClick={() => getSingleTrackDetails(trackId)}
          to={`/details/${trackId}`}
        >
          Details
        </StyledLinkedButton>
      );
    }
  };

  const additionalInfo = () => {
    if (props.isDetailsPage) {
      return (
        <div>
          <div>
            <strong> Song Duration : </strong>
            {convertFromMilliseconds(trackTimeMillis)}
          </div>
          <div>
            <strong> Release Date:</strong>
            {convertDate(releaseDate)}
          </div>
        </div>
      );
    }

    return null;
  };

  return (
    <MainContainer>
      <DetailsContainer isDetailsPage={isDetailsPage}>
        <div>
          <strong>Song:</strong> {trackName}
        </div>
        <div>
          <strong>Artist:</strong> {artistName}
        </div>
        <div>
          <strong>Price:</strong> {currency} {trackPrice}
        </div>
        {additionalInfo()}
      </DetailsContainer>
      <ImageAndBtnContainer>
        <img src={props.artworkUrl100} alt="img" />
        {detailsButton()}
      </ImageAndBtnContainer>
    </MainContainer>
  );
}

export default TrackDetails;
