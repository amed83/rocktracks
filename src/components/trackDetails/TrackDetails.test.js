import TrackDetails from "./TrackDetails";

describe("TrackDetails ", () => {
  const loadData = (props) => {
    return shallow(<TrackDetails {...props} />);
  };

  test("render the standard button when the component is rendering the details view", () => {
    const props = {
      isDetailsPage: true,
    };
    const wrapper = loadData(props);
    expect(wrapper.find('[data-test="normal-btn"]')).toHaveLength(1);
  });

  test("render the linked button when the component is rendering the view all page ", () => {
    const props = {
      isDetailsPage: null,
    };
    const wrapper = loadData(props);
    expect(wrapper.find('[data-test="linked-btn"]')).toHaveLength(1);
  });
});
