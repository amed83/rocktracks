import styled, { css } from "styled-components";
import { Link } from "react-router-dom";

export const MainContainer = styled.div`
  display: flex;
  margin-bottom: 1rem;
  flex-direction: column;
  padding: 1rem;
  align-items: flex-start;
  width: 20%;
  flex-wrap: wrap;
`;
export const DetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem 0;
  height: ${(props) => (props.isDetailsPage ? "auto" : "7rem")};

  > div {
    line-height: 2;
  }
`;

export const ImageAndBtnContainer = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  height: 11rem;
  justify-content: space-around;
`;

const ButtonProps = css`
  padding: 1rem;
  font-size: 18px;
  background-color: #95959a;
  cursor: pointer;
  border-radius: 5px;
  text-decoration: none;
  color: black;
  text-align: center;
`;

export const StyledLinkedButton = styled(Link)`
  ${ButtonProps}
`;

export const StyledButton = styled.button`
  ${ButtonProps}
`;
