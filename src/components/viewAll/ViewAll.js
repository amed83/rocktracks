import React from "react";
import PropTypes from "prop-types";
import { MainContainer } from "./style";

import TrackDetails from "../trackDetails/TrackDetails";
const propTypes = {
  tracksList: PropTypes.arrayOf(
    PropTypes.shape({
      trackId: PropTypes.number,
      trackName: PropTypes.string,
      artistName: PropTypes.string,
      trackPrice: PropTypes.number,
      artworkUrl60: PropTypes.string,
    })
  ),
  getSingleTrack: PropTypes.func,
  getTrackDetails: PropTypes.func,
};

export function ViewAll({
  tracksList,
  getSingleTrackDetails,
  isLoading,
  alertMessage,
}) {
  return (
    <MainContainer>
      {!isLoading &&
        tracksList &&
        tracksList.map(
          ({
            trackId,
            trackName,
            artistName,
            trackPrice,
            artworkUrl100,
            currency,
          }) => {
            return (
              <TrackDetails
                key={trackId}
                trackId={trackId}
                trackName={trackName}
                artistName={artistName}
                trackPrice={trackPrice}
                artworkUrl100={artworkUrl100}
                getSingleTrackDetails={getSingleTrackDetails}
                currency={currency}
                data-test="trackItem-component"
              />
            );
          }
        )}
      {!isLoading && !alertMessage && tracksList && tracksList.length < 1 && (
        <h2 data-test="nothingToShow-message">Nothing to show, sorry :(</h2>
      )}
    </MainContainer>
  );
}

ViewAll.propTypes = propTypes;

export default ViewAll;
