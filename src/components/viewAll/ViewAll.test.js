import { ViewAll } from "./ViewAll";

describe("ViewAll ", () => {
  const loadData = (props) => {
    return shallow(<ViewAll {...props} />);
  };

  test("renders TrackDetails component when tracksList is loaded", () => {
    const props = {
      tracksList: [{ trackId: 1 }],
    };
    const wrapper = loadData(props);
    expect(wrapper.find('[data-test="trackItem-component"]')).toHaveLength(1);
    expect(wrapper.find('[data-test="nothingToShow-message"]')).toHaveLength(0);
  });

  test("shows a 'Nothing to show' message is the trackList is empty ", () => {
    const props = {
      tracksList: [],
    };
    const wrapper = loadData(props);
    expect(wrapper.find('[data-test="nothingToShow-message"]')).toHaveLength(1);
    expect(wrapper.find('[data-test="trackItem-component"]')).toHaveLength(0);
  });
});
