export const ALL_TRACKS_URL = `https://itunes.apple.com/search?term=rock&media=music`;

export const ROUTES = {
  VIEW_ALL: {
    path: "/",
  },
  TRACK_DETAILS: {
    path: "/details/:trackId",
  },
};

export const STANDARD_ERROR_MESSAGE = "Something went wrong, please try again";
