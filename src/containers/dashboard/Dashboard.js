import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
  getAllTracks,
  getSingleTrack,
} from "../../store/actions/tracks/tracksActions";
import { Switch, Route } from "react-router-dom";
import ViewAll from "../../components/viewAll/ViewAll";
import { ROUTES } from "../../constants/constants";
import AlertMessage from "../../components/alertMessage/AlertMessage";
import PropTypes from "prop-types";
import TrackDetails from "../../components/trackDetails/TrackDetails";
import { BrowserRouter as Router } from "react-router-dom";

const defaultProps = {
  getAllTracks: () => {},
};

const propTypes = {
  getAllTracks: PropTypes.func.isRequired,
  alertMessage: PropTypes.string,
  isLoading: PropTypes.bool,
  tracksList: PropTypes.array,
  isDetailsPage: PropTypes.string,
};

export function Dashboard({
  getAllTracks,
  alertMessage,
  isLoading,
  tracksList,
  trackDetails,
  getSingleTrack,
}) {
  useEffect(() => {
    if (tracksList.length < 1) {
      getAllTracks();
    }
  }, []);

  const messageToDisplay = isLoading ? (
    <h2 data-test="loading-message">Loading</h2>
  ) : alertMessage ? (
    <AlertMessage alertMessage={alertMessage} data-test="alert-message" />
  ) : null;

  const getSingleTrackDetails = (trackId) => {
    getSingleTrack(trackId);
  };

  return (
    <div style={styles}>
      {messageToDisplay}
      <Router>
        <Switch>
          {!messageToDisplay && (
            <Route
              data-test="details-route"
              exact
              path={ROUTES.TRACK_DETAILS.path}
              render={() => (
                <TrackDetails
                  trackName={trackDetails.trackName}
                  artworkUrl100={trackDetails.artworkUrl100}
                  artistName={trackDetails.artistName}
                  currency={trackDetails.currency}
                  trackPrice={trackDetails.trackPrice}
                  trackTimeMillis={trackDetails.trackTimeMillis}
                  releaseDate={trackDetails.releaseDate}
                  trackViewUrl={trackDetails.trackViewUrl}
                  isDetailsPage={true}
                />
              )}
            />
          )}
          {!messageToDisplay && (
            <Route
              exact
              path={ROUTES.VIEW_ALL.path}
              render={() => (
                <ViewAll
                  alertMessage={alertMessage}
                  isLoading={isLoading}
                  tracksList={tracksList}
                  trackDetails={trackDetails}
                  getSingleTrackDetails={getSingleTrackDetails}
                />
              )}
            />
          )}
        </Switch>
      </Router>
    </div>
  );
}

const styles = {
  display: "flex",
  justifyContent: "center",
};

Dashboard.defaultProps = defaultProps;
Dashboard.propTypes = propTypes;

export default connect(
  (state) => ({
    alertMessage: state.alertMessage.message,
    isLoading: state.tracks.loading,
    tracksList: state.tracks.tracksList,
    trackDetails: state.tracks.selectedTrack,
  }),
  {
    getAllTracks,
    getSingleTrack,
  }
)(Dashboard);
