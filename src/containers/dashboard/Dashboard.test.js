import { Dashboard } from "./Dashboard";

describe("Dashboard ", () => {
  const loadData = (props) => {
    return shallow(<Dashboard {...props} />);
  };

  test("renders AlertMessage component ", () => {
    const wrapper = loadData({ alertMessage: "some error " });
    expect(wrapper.find('[data-test="alert-message"]')).toHaveLength(1);
  });

  test("show loading message", () => {
    const wrapper = loadData({ isLoading: true });
    expect(wrapper.find('[data-test="loading-message"]')).toHaveLength(1);
  });

  test("renders the route to the details component when there is no error or loading message", () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.find('[data-test="details-route"]')).toHaveLength(1);
  });
});
