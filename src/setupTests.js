import React from "react";
import { configure, shallow, mount, render } from "enzyme";

global.React = React;
global.render = render;
global.shallow = shallow;
global.mount = mount;
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
