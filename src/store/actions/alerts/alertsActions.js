import * as actionTypes from "../actionTypes";

export const setErrorMessage = (payload) => ({
  type: actionTypes.SET_ERROR_MESSAGE,
  payload,
});
