import configureMockStore from "redux-mock-store";
import * as actions from "./alertsActions";
import { SET_ERROR_MESSAGE } from "../actionTypes";

describe("alerts actions ", () => {
  const mockStore = configureMockStore();
  let store;

  store = mockStore({ message: null });

  test("setErrorMessage return SET_ERROR_MESSAGE actin ", async () => {
    const expectActions = [{ type: SET_ERROR_MESSAGE }];
    await store.dispatch(actions.setErrorMessage());
    expect(store.getActions()).toEqual(expectActions);
  });
});
