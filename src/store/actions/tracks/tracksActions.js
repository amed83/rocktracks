import * as actionTypes from "../actionTypes";

export const getAllTracks = () => ({
  type: actionTypes.GET_ALL_TRACKS,
});

export const getAllTracksSuccess = (payload) => ({
  type: `${actionTypes.GET_ALL_TRACKS}_SUCCESS`,
  payload,
});

export const getAllTracksFail = () => ({
  type: `${actionTypes.GET_ALL_TRACKS}_FAIL`,
});

export const getSingleTrack = (payload) => ({
  type: actionTypes.GET_SINGLE_TRACK,
  payload,
});
