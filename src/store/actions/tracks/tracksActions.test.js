import configureMockStore from "redux-mock-store";
import * as actions from "./tracksActions";
import { GET_ALL_TRACKS, GET_SINGLE_TRACK } from "../actionTypes";

describe("tracks actions", () => {
  const mockStore = configureMockStore();

  let store;

  beforeEach(() => {
    store = mockStore({ tracksList: null, selectedTrack: null });
  });

  test("getAllTracks returns GET_ALL_TRACKS action ", async () => {
    const expectedActions = [{ type: GET_ALL_TRACKS }];
    await store.dispatch(actions.getAllTracks());
    expect(store.getActions()).toEqual(expectedActions);
  });

  test("getAllTracks returns GET_ALL_TRACKS_SUCCESS action ", async () => {
    const expectedActions = [{ type: `${GET_ALL_TRACKS}_SUCCESS` }];
    await store.dispatch(actions.getAllTracksSuccess());
    expect(store.getActions()).toEqual(expectedActions);
  });

  test("getAllTracks returns GET_ALL_TRACKS action ", async () => {
    const expectedActions = [{ type: `${GET_ALL_TRACKS}_FAIL` }];
    await store.dispatch(actions.getAllTracksFail());
    expect(store.getActions()).toEqual(expectedActions);
  });

  test("getSingleTrack returns GET_SINGLE_TRACK action ", async () => {
    const expectedActions = [{ type: GET_SINGLE_TRACK }];
    await store.dispatch(actions.getSingleTrack());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
