import * as actionTypes from "../../actions/actionTypes";

const INITIAL_STATE = {
  message: null,
};

const alertsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.SET_ERROR_MESSAGE:
      return {
        ...state,
        message: action.payload,
      };

    default:
      return state;
  }
};

export default alertsReducer;
