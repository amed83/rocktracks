import alertsReducer from "./alertsReducer";

import { SET_ERROR_MESSAGE } from "../../actions/actionTypes";
const INITIAL_STATE = {
  message: null,
};

describe("alertsReducer", () => {
  test("SET_ERROR_MESSAGE add an error message", () => {
    expect(
      alertsReducer(
        {
          ...INITIAL_STATE,
        },
        {
          type: SET_ERROR_MESSAGE,
          payload: "some error happened",
        }
      )
    ).toEqual({
      ...INITIAL_STATE,
      message: "some error happened",
    });
  });
});
