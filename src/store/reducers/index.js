import { combineReducers } from "redux";
import tracksReducer from "./tracks/tracksReducer";
import alertsReducer from "./alerts/alertsReducer";
import { persistReducer } from "redux-persist";
import storageSession from "redux-persist/lib/storage/session";

const persistConfig = {
  key: "tracks",
  storage: storageSession,
  whitelist: ["selectedTrack"],
};

export default combineReducers({
  tracks: persistReducer(persistConfig, tracksReducer),
  alertMessage: alertsReducer,
});
