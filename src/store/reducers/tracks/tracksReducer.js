import * as actionTypes from "../../actions/actionTypes";

const INITIAL_STATE = {
  tracksList: [],
  selectedTrack: null,
  loading: false,
};

const tracksReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_TRACKS:
      return {
        ...state,
        loading: true,
      };

    case `${actionTypes.GET_ALL_TRACKS}_SUCCESS`:
      return {
        ...state,
        tracksList: [...action.payload],
        loading: false,
      };

    case `${actionTypes.GET_ALL_TRACKS}_FAIL`:
      return {
        ...state,
        loading: false,
      };

    case `${actionTypes.GET_SINGLE_TRACK}`:
      return {
        ...state,
        selectedTrack: state.tracksList.filter(
          (item) => item.trackId === action.payload
        )[0],
      };

    default:
      return state;
  }
};

export default tracksReducer;
