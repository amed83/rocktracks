import tracksReducer from "./tracksReducer";

import { GET_ALL_TRACKS, GET_SINGLE_TRACK } from "../../actions/actionTypes";

const INITIAL_STATE = {
  tracksList: null,
  selectedTrack: null,
  loading: false,
};

describe("trackReducer ", () => {
  test("returns the default state ", () => {
    expect(tracksReducer(INITIAL_STATE, {})).toEqual(INITIAL_STATE);
  });

  test("GET_ALL_TRACKS : the loading flag is set to true", () => {
    expect(
      tracksReducer(
        {
          ...INITIAL_STATE,
        },
        {
          type: GET_ALL_TRACKS,
        }
      )
    ).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  test("GET_ALL_TRACKS_SUCCESS : the payload with the tracks data is added to the list", () => {
    expect(
      tracksReducer(
        { ...INITIAL_STATE },
        {
          type: `${GET_ALL_TRACKS}_SUCCESS`,
          payload: [
            {
              trackId: "1377813701",
              trackName: "Sweet child o Mine",
              artistName: "Guns N Roses",
            },
          ],
        }
      )
    ).toEqual({
      ...INITIAL_STATE,
      tracksList: [
        {
          trackId: "1377813701",
          trackName: "Sweet child o Mine",
          artistName: "Guns N Roses",
        },
      ],
      loading: false,
    });
  });

  test("GET_SINGLE_TRACK return the selected track data an add it to selectedTrack", () => {
    expect(
      tracksReducer(
        { ...INITIAL_STATE, tracksList: [{ trackId: 123 }, { trackId: 234 }] },
        {
          type: "GET_SINGLE_TRACK",
          payload: 123,
        }
      )
    ).toEqual({
      ...INITIAL_STATE,
      tracksList: [{ trackId: 123 }, { trackId: 234 }],
      selectedTrack: { trackId: 123 },
    });
  });
});
