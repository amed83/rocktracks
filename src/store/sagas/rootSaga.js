import { all } from "redux-saga/effects";
import { watchTracks } from "./tracks/tracksWatcher";

export default function* rootSaga() {
  yield all([watchTracks()]);
}
