import { takeEvery } from "redux-saga/effects";
import * as actionTypes from "../../actions/actionTypes";
import { tracksWorker } from "./tracksWorker";

export function* watchTracks() {
  yield takeEvery(actionTypes.GET_ALL_TRACKS, tracksWorker);
}
