import { takeLatest } from "redux-saga/effects";
import * as actionTypes from "../../actions/actionTypes";

import { tracksWorker } from "./tracksWorker";
import { watchTracks } from "./tracksWatcher";

describe("watchTracks", () => {
  const watchIterator = watchTracks();
  test("watches GET_ALL_TRACKS action with tracksWorker ", () => {
    expect(JSON.stringify(watchIterator.next().value)).toEqual(
      JSON.stringify(takeLatest(actionTypes.GET_ALL_TRACKS, tracksWorker))
    );
  });
});
