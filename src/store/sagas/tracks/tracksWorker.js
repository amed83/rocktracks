import { call, put } from "redux-saga/effects";
import { api } from "../../../utils/api/api";
import { STANDARD_ERROR_MESSAGE } from "../../../constants/constants";

import {
  getAllTracksSuccess,
  getAllTracksFail,
} from "../../actions/tracks/tracksActions";

import { setErrorMessage } from "../../actions/alerts/alertsActions";

export function* tracksWorker() {
  let response;
  try {
    response = yield call(api);
  } catch (err) {
    yield put(getAllTracksFail());
    return yield put(setErrorMessage(STANDARD_ERROR_MESSAGE));
  }

  yield put(getAllTracksSuccess(response.data.results));
}
