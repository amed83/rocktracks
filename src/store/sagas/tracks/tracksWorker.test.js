import { tracksWorker } from "./tracksWorker";
import { call, put } from "redux-saga/effects";
import { api } from "../../../utils/api/api";
import { STANDARD_ERROR_MESSAGE } from "../../../constants/constants";

import { setErrorMessage } from "../../actions/alerts/alertsActions";
import {
  getAllTracksSuccess,
  getAllTracksFail,
} from "../../actions/tracks/tracksActions";

describe("tracksWorker ", () => {
  let tracksWorkerIterator;

  beforeEach(() => {
    tracksWorkerIterator = tracksWorker();
  });

  test("performs  an api call", () => {
    expect(tracksWorkerIterator.next().value).toEqual(call(api));
  });

  test("in case of success dispatch getAllTracksSuccess action ", () => {
    tracksWorkerIterator.next();

    const response = {
      data: {
        results: [],
      },
    };
    expect(JSON.stringify(tracksWorkerIterator.next(response).value)).toEqual(
      JSON.stringify(put(getAllTracksSuccess(response.data.results)))
    );
  });

  test(" in case of error dispatch two actions,getAllTracksFail and setErrorMessage ", () => {
    tracksWorkerIterator.next();

    const error = tracksWorkerIterator.throw(new Error("mock error")).value;
    expect(JSON.stringify(error)).toEqual(
      JSON.stringify(put(getAllTracksFail()))
    );
    expect(JSON.stringify(tracksWorkerIterator.next().value)).toEqual(
      JSON.stringify(put(setErrorMessage(STANDARD_ERROR_MESSAGE)))
    );
  });
});
