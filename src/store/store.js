import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { persistStore, persistReducer } from "redux-persist";
import storageSession from "redux-persist/lib/storage/session";
import rootReducer from "./reducers/index";
import rootSaga from "./sagas/rootSaga";

const persistConfig = {
  key: "app",
  storage: storageSession,
  blacklist: ["alertMessage", "tracks"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const saga = createSagaMiddleware();
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
  persistedReducer,
  composeEnhancer(applyMiddleware(saga))
);

saga.run(rootSaga);

export const persistor = persistStore(store);
