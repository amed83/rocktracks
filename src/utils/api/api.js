import axios from "axios";
import { ALL_TRACKS_URL } from "../../constants/constants";

export async function api() {
  return axios.get(ALL_TRACKS_URL);
}
