import { convertFromMilliseconds, convertDate } from "./convertTime";

test("convertFromMilliseconds converts milliseconds to to minutes and seconds format  ", () => {
  expect(convertFromMilliseconds(356067)).toEqual("5:56");
});

test("convertDate converts date from UTC format month/day/year format", () => {
  expect(convertDate("1987-07-21T12:00:00Z")).toEqual("7/21/1987");
});
